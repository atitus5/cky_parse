println("Initializing parser...")

#USE SHARED_CKY.JL AND SHARED_TEST.JL - FAR MORE EFFICIENT

#Start timing of initialization
before = time()

#Preload files on all processes
require("cky.jl")
require("reduce.jl")

#Check if a grammar was specified
if length(ARGS) < 1
    error("Please specify a grammar as an argument.")
end

grammar_file = ARGS[1]

#Parse grammar file into array of Rule types
grammar = parse_grammar(grammar_file)

#Receive sentence to parse
#print("Please enter a sentence: ")
#sentence = readline(STDIN)

#Test sentences for larger_dense.grammar
sentence = "he is a man she is a woman he is he is a man she is a woman he is"
#sentence = "she is a woman"

#Split sentence into words
tokens = split(replace(sentence, "\n", ""), " ")
n = length(tokens)

#Check if grammar can parse sentence
check_coverage(tokens, grammar)

#Set start symbol to be first word in grammar
#ASSUMES START SYMBOL IS SYMBOL IN FIRST RULE OF GRAMMAR FILE
start_word = grammar[1].nonterminal

#Build string-to-int lookup table for terminals and nonterminals in grammar
lookup_table = build_lookup_table(grammar, tokens)

@printf "Parser initialized in %.4f seconds\n" (time() - before) 

println("Parsing...")

#Start timing of parsing
before = time()

parse_table = parse(grammar, tokens, n, start_word, lookup_table)
@printf "Sentence parsed in %.4f seconds\n" (time() - before) 

#Optional printing of parse table
#print_table(parse_table, lookup_table)


#LARGE BATCH TESTING
#x = 100

#while x > 0
#	parse_table = parse(grammar, tokens, n, start, lookup_table)
#	x -= 1
#end
#@printf "100 sentences of length 20 parsed in %.4f seconds\n" (time() - before) 
println("done!")
