CKY Parsing using Julia
=======================

This version uses Julia 0.3.3 and the (currently) experimental ` SharedArray ` feature.
Please note for future versions.

This directory contains several different files useful for conducting CKY
parsing in Julia. Please note that ` cky.jl `, ` reduce.jl `, and ` test.jl ` are obsolete;
please use ` shared_cky.jl ` and ` shared_test.jl ` as replacement.

Using ` SharedArray `s, we were able to achieve a steady-state ~25% speedup per sentence when
run on 4 processors as compared to 1 processor.

Contents
--------

  * ` cky.jl ` 					      - (OBSOLETE) Probabilistic CKY parsing and other NLP utilities
  * ` custom_types.jl `			  - Type definitions
  * ` dense.grammar ` 			  - Simple dense CFG in CNF
  * ` larger_dense.grammar ` 	- Larger dense CFG in CNF
  * ` make_grammar.py `			  - Sample dense grammar generation script
  * ` reduce.jl `				      - (OBSOLETE) Reduction functions for use with parallel for loops
  *	` shared_cky.jl ` 			  - New version of ` cky.jl `, using SharedArray feature
  * ` shared_test.jl ` 			  - New version of ` test.jl `, using SharedArray feature
  * ` test.jl ` 				      - (OBSOLETE) Simple testing suite for CKY parsing
  * ` toy.grammar `				    - Simple toy CFG in CNF
  * ` table_ints.jpg `        - Diagrams of how the ` table_ints ` `SharedArray` is stored in memory and indexed
  * ` table_floats.jpg `      - Diagrams of how the ` table_floats ` `SharedArray` is stored in memory and indexed
  * ` README.md `             - This file.

Usage
-----

To parse, run ` julia -p < # of processes desired > shared_test.jl < grammar file desired > `.

One can either wait for a user-input sentence or use a predefined sentence by setting the
` sentence ` variable, as demonstrated by default with a (nonsensical) sentence for the
` larger_dense.grammar 	` file.

Table structure
---------------

When refactoring the code to make ` SharedArray ` usage possible, we had to separate the
table into two arrays, one for integers and one for floats, in order to comply with the
current restrictions on ` SharedArray ` element types. The indexing is complicated
(as evidenced by the many offset variables needed in ` shared_cky.jl `), so I have included
a rough diagram of what the arrays look like in memory:

` table_ints `:
  This is a three dimensional array,
  accessed by ` [x, column, nonterminal (as an integer, using stoi_table[nonterminal])] `.
  Columns and nonterminals are indexed normally, but the variable ` x ` allows us to store the
  following data for each row, column, nonterminal entry:
    ` [filled (1 or 0), current_index (used for adding children), x_1, y_1, z_1, x_2, y_2, z_2, ... ] `
  where the subscripts for ` x `, ` y `, and ` z ` denote which child of that entry
  is being referred to, and ` current_index ` is the subscript for the next child to be added.
  Offsets are calculated in ` shared_cky.jl ` to make it possible to know which field in the above
  array is being accessed or modified, and for which row.

` table_floats `:
  This is also a three dimensional array, accessed in the same way. However, ` x ` now allows us
  to store the following data for each row, column, nonterminal entry:
    ` [probability of entry, probability of first child, probability of second child, ...] `
  Offsets are also calculated for this table to make access easier.

To conceptualize this visually, please consult ` table_ints.jpg ` and ` table_floats.jpg `.