import random

#Generator script for larger_dense.grammar - can be modified for generating new dense grammars
nonterminals = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']

f = open("larger_dense.grammar", 'w')
for a in nonterminals:
	for b in nonterminals:
		for c in nonterminals:
			f.write("%s %s %s %f\n" % (a,b,c,random.random()))
f.close()