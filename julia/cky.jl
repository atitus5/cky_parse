@everywhere require("custom_types.jl")

#USE SHARED_CKY.JL AND SHARED_TEST.JL - FAR MORE EFFICIENT

#Given grammar and a terminal, return all tags for the word
function tags(grammar, lhs)
    possibilities = Rule[]
    for rule in grammar
        if rule.left_child == lhs
            push!(possibilities, rule)
        end
    end
    return possibilities
end

#Parse grammar into a Rule array
function parse_grammar(grammar_file)
    #Create array of Rules for grammar
    cfg = Rule[]            

    #Open grammar file
    f = open(grammar_file)

    #Iterate over all lines in grammar file
    for line in eachline(f)
        #Parse line
        new_parsed = split(replace(line, "\n", ""), " ")

        #Create new Rule from parts of parsed line
        new_rule = Rule(new_parsed[1], new_parsed[2], new_parsed[3], float64(new_parsed[4]))

        #Add new rule to cfg
        push!(cfg, new_rule)
    end
    return cfg
end

#Check if grammar can parse all tokens in sentence
function check_coverage(tokens, grammar)
    #Initialize terminal lookup table
    terminals = Dict{ASCIIString, Bool}()

    #Trim grammar to terminals
    for rule in grammar
        #Check if right child is None - indicating a terminal
        if rule.right_child == "None"
            terminals[rule.left_child] = true
        end
    end

    #Check what tokens the grammar does not cover and print them
    covered = true
    for token in tokens
        if haskey(terminals, token) == false
            println(token)
            covered = false
        end
    end

    #Raise error if terminals are not covered
    if covered == false
        error("Grammar does not cover the terminals listed above.")
    end
end

#Pretty printing of parse table
function print_table(parse_table, lookup_table)
    #Iterate through table
    for j = 1:n+1
        for i = 1:n
            empty = true

            #Check all nonterminals
            for nonterminal in keys(lookup_table)
                k = lookup_table[nonterminal]

                #Check if entry for given nonterminal is filled
                if parse_table[i,j,k].filled == true
                    empty = false
                    print(nonterminal)
                    print(" ")
                end
            end

            #If none are filled, print a dot
            if empty
                print(".")
            end
            print("\t")
        end
        println()
    end
    println()
end

#Build string-to-int lookup table for all nonterminals and terminals in grammar
function build_lookup_table(grammar, tokens)
    #Initialize lookup table
    lookup_table = Dict{ASCIIString, Int64}()
    count = 1

    for rule in grammar
        #Check if nonterminal is repeated
        try
            temp = lookup_table[rule.nonterminal]
        catch KeyError
            lookup_table[rule.nonterminal] = count
            count += 1
        end

        #Add terminal if applicable
        if rule.right_child == "None"
            try
                temp = lookup_table[rule.left_child]
            catch KeyError
                lookup_table[rule.left_child] = count
                count += 1
            end
        end
    end

    return lookup_table
end

#Fill table entries
function fill_entry(i, j, table, lookup_table, tokens, grammar)
    #Initialize return array of Entry types
    new_entries = Array(Entry, length(lookup_table))

    #Initialize possibilities array and set it as new_entries[x]
    for x = 1:length(lookup_table)
        possibilities = Array((Int64,Int64,Int64,Float64), length(tokens)*(length(lookup_table)^2))
        new_entries[x] = Entry(true, 1.0, 1, possibilities)
    end

    #Cover all span splits between first and second child
    for x=1:j-2
        #Check all filled nonterminals for first
        for a=1:length(lookup_table)
            if table[i-j+x+1,x+1,a].filled == true
                #Check all filled nonterminals for second
                for b=1:length(lookup_table)
                    if table[i,j-x,b].filled == true
                        #Find all rules
                        for rule in grammar
                            if !(rule.nonterminal == start && (i,j) != (n,n+1)) && rule.right_child != "None"
                                if lookup_table[rule.left_child] == a && lookup_table[rule.right_child] == b
                                    #Find probability of children
                                    prob1 = table[i-j+x+1,x+1,a].prob
                                    prob2 = table[i,j-x,b].prob

                                    #Create new probability
                                    new_prob = rule.prob * prob1 * prob2

                                    #Update return parameters
                                    index = lookup_table[rule.nonterminal]
                                    new_entries[index].prob *= new_prob
                                    new_entries[index].possibilities[new_entries[index].current_index] = (a,b,x,new_prob)
                                    new_entries[index].current_index += 1 
                                end
                            end
                        end
                    end
                end
            end
        end
    end
    return new_entries
end

#Fill entries in parallel using refactored parallel for loop
function parallel_fill(i_low, i_high, j, table, lookup_table, n, grammar)
    entry_list = @parallel ret_array for i=i_low:i_high
        #Create return array
        new_entries = Array(Entry, length(lookup_table))
        for x = 1:length(lookup_table)
            possibilities = Array((Int64,Int64,Int64,Float64), n*(length(lookup_table)^2))
            new_entries[x] = Entry(true, 1.0, 1, possibilities)
        end

        #Cover all span splits between first and second child
        for x=1:j-2
            #Check all filled nonterminals for first
            for a=1:length(lookup_table)
                if table[i-j+x+1,x+1,a].filled == true
                    #Check all filled nonterminals for second
                    for b=1:length(lookup_table)
                        if table[i,j-x,b].filled == true
                            #Find all rules
                            for rule in grammar
                                if !(rule.nonterminal == start && (i,j) != (n,n+1)) && rule.right_child != "None"
                                    if lookup_table[rule.left_child] == a && lookup_table[rule.right_child] == b
                                        #Find probability of children
                                        prob1 = table[i-j+x+1,x+1,a].prob
                                        prob2 = table[i,j-x,b].prob

                                        #Create new probability
                                        new_prob = rule.prob * prob1 * prob2

                                        #Update return parameters
                                        index = lookup_table[rule.nonterminal]
                                        new_entries[index].prob *= new_prob
                                        new_entries[index].possibilities[new_entries[index].current_index] = (a,b,x,new_prob)
                                        new_entries[index].current_index += 1 
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
        new_entries
    end

    #Account for if only one entry is in entry list
    if ndims(entry_list) == 1
        temp = Array(Array{Entry, 1}, 1, 1)
        temp[1,1] = entry_list
        return temp
    else
        return entry_list
    end
end

#Probabilistic CKY Parse
function parse(grammar, tokens, n, start, lookup_table) 
    #Initialize table
    table = Array(Entry, (n, n+1, length(lookup_table)))

    #Preallocate entries to be marked as non-filled, prob 1.0 Entry types with unfilled possibility arrays
    for x=1:n
        for y=1:n+1
            for z=1:length(lookup_table)
                table[x,y,z] = Entry(false, 1.0, 1, Array((Int64, Int64, Int64, Float64), length(tokens)*(length(lookup_table)^2)))
            end
        end
    end

    #Initial setup
    for y = 1:n
        #Fill terminals - mark as filled, add possibility, increment index
        table[y, 1, lookup_table[tokens[y]]].filled = true
        table[y, 1, lookup_table[tokens[y]]].prob = 1.0
        table[y, 1, lookup_table[tokens[y]]].possibilities[table[y, 1, lookup_table[tokens[y]]].current_index] = (0,0,0,1.0)
        table[y, 1, lookup_table[tokens[y]]].current_index += 1

        #Fill initial tags
        rules = tags(grammar, tokens[y])
        for rule in rules
            table[y,2,lookup_table[rule.nonterminal]].filled = true
            table[y,2,lookup_table[rule.nonterminal]].prob *= rule.prob
            table[y,2,lookup_table[rule.nonterminal]].possibilities[table[y,2,lookup_table[rule.nonterminal]].current_index] = (lookup_table[tokens[y]], 0, 1, rule.prob)
            table[y,2,lookup_table[rule.nonterminal]].current_index += 1
        end
    end

    #Move across columns
    for j = 3:n+1
        #Iterate down the row in parallel
        #Pretty slow, especially in parallel
        #Use the newer shared_cky.jl for far better performance

        #Collect results from parallel fill
        results = parallel_fill(j-1, n, j, table, lookup_table, n, grammar)
        row = j-1

        #Place results in table
        for result in results
            for nonterminal = 1:length(lookup_table)
                table[row, j, nonterminal] = result[nonterminal]
            end
            row += 1
        end
    end

    return table
end
