CKY Parsing Algorithm for Natural Language Processing
=====================================================

Python2.7.6 and Julia0.3.3 implementations of the common CKY (also written as CYK) natural language parsing algorithm

Please use the Julia version. It is more efficient in general, but is also capable of
being run on dense grammars, utilizes parallelization, and does not depend on a third party library.

See the READMEs in python/ and julia/ for more information on running their respective programs

Dependencies
------------

* `nltk 3.0.0` for Python 

Contents
--------

  * ` python/cky.py ` 				- Probabilistic CKY parsing and other NLP utilities 
  * ` python/test.py ` 				- Simple testing suite for CKY parsing
  * ` python/toy.cfg `				- Simple toy CFG in CNF
  * ` python/toy.pcfg `				- Simple toy PCFG in CNF
  * ` julia/cky.jl ` 				- Probabilistic CKY parsing and other NLP utilities
  * ` julia/custom_types.jl `		- Type definitions
  * ` julia/dense.grammar ` 		- Simple dense CFG in CNF
  * ` julia/larger_dense.grammar ` 	- Larger dense CFG in CNF
  * ` julia/make_grammar.py `		- Sample dense grammar generation script
  * ` julia/reduce.jl `				- Reduction functions for use with parallel for loops
  *	` julia/shared_cky.jl ` 		- Version of ` cky.jl `, using ` SharedArray ` feature
  * ` julia/shared_test.jl ` 		- Version of ` test.jl `, using ` SharedArray ` feature
  * ` julia/test.jl ` 				- Simple testing suite for CKY parsing
  * ` julia/toy.grammar `			- Simple toy CFG in CNF
  * ` README.md `					- This file
