import nltk.data
import nltk.grammar
import nltk.tree
import time

class TableEntry:
    #Initialize entry in parse table
    def __init__(self):
		self.entry = []
		self.probs = {}

    def __getitem__(self, key):
		return self.entry[key]

    def __str__(self):
		return " ".join([str(symbol) for symbol in self.symbols])

    def add_entry(self, new_entry):
		self.entry.append(new_entry)
		self.symbols = []
		for x in self.entry:
			#Fill self.symbols
		    if x.keys()[0] not in self.symbols:
				self.symbols.append(x.keys()[0])

    def set_prob(self, key, prob):
		self.probs[key] = prob

    def prob(self, key):
		return self.probs[key]

	#Get back pointers
    def get_pointers(self, nonterminal):
		pointers = []
		for y in self.entry:
		    if y.keys()[0] == nonterminal:
			pointers.append(y[nonterminal][1])
		return pointers
    
class ParseTable:
    #Initialize parse table with tokens
    def __init__(self, tokens):
		self.table = dict()
		self.n = len(tokens)
		for i in xrange(len(tokens)):
		    self.table[(self.n,i+1)] = TableEntry()
		    self.table[(self.n,i+1)].add_entry({tokens[i]: [(None, None)]})
    
    def __setitem__(self, key, entry):
		self.table[key] = entry

    def __getitem__(self, key):
		return self.table[key]

	#Pretty print of table
    def __str__(self):
		full = ""
		count = 1
		for x in xrange(0, self.n + 1):
		    for y in xrange(1, self.n + 1):
				try:
				    full += str(self.table[(x,y)])
				except KeyError:
				    full += '.'
				full += "\t"
		    full += "\n"
		return full

    #Find all possible parse trees
    def enumerate_trees(self, row, col, nonterminal):
		#Get child pointers in form of a list of two tuples [((a,b),(c,d))]
		pointers = self.table[(row,col)].get_pointers(nonterminal)
		all_trees = []

		#Iterate over all child pointers
		for p in pointers:
		    #Get left child in form of a list of a pointer tuple and nonterminal [(a,b),symbol]
		    for x in self.table[(row,col)].entry:
				try:
				    symbol = x[nonterminal][0].rhs()[0]
				    if symbol in self.table[p[0]].symbols:
					break	
				except KeyError:
				    continue

		    left_p = [p[0], symbol]

		    tree = []

		    #Check if right child is None, indicating that the terminal has been reached
		    if p[1] == None:
				tree = [nonterminal, left_p[1]]
				all_trees.append(tree)
		    else:
				#Get right child in same form as left child
				for x in self.table[(row,col)].entry:
				    try:
						symbol = x[nonterminal][0].rhs()[1]
						if symbol in self.table[p[1]].symbols:
						    break	
				    except KeyError:
						continue
				right_p = [p[1], symbol]
				left_child_trees = self.enumerate_trees(left_p[0][0], left_p[0][1], left_p[1])
				right_child_trees = self.enumerate_trees(right_p[0][0], right_p[0][1], right_p[1])

				#Construct all trees in form of lists
				for l_tree in left_child_trees:
				    if not isinstance(l_tree, nltk.grammar.Nonterminal):
						for r_tree in right_child_trees:
						    if not isinstance(r_tree, nltk.grammar.Nonterminal):
								all_trees.append([nonterminal, l_tree, r_tree])
		return all_trees

    #Produces visualizable version of trees
    def visualize_trees(self, start):
		trees = self.enumerate_trees(0, self.n, start)
		tree_strs = []
		for tree in trees:
		    tree_strs.append(str(tree).replace(",","").replace("\'","").replace("[","(").replace("]",")"))
		for tree_str in tree_strs:
		    x = nltk.tree.Tree.fromstring(tree_str).draw()
    
    #Produces visualizable version of best tree
    def visualize_best(self, start):
		trees = self.enumerate_trees(0, self.n, start)
		print len(trees)
		tree = trees[0]
		tree_str = str(tree).replace(",","").replace("\'","").replace("[","(").replace("]",")")
		nltk.tree.Tree.fromstring(tree_str).draw()

def cky_parse(grammar, tokens):
    #Initialize parse table
    table = ParseTable(tokens)

    #Move left to right across table
    for j in xrange(1, table.n + 1):
		#Add nonterminal symbols that correspond to terminal symbol
		rules = grammar.productions(rhs = tokens[j-1])
		table[(j-1,j)] = TableEntry()
		for rule in rules:
		    new_rule = nltk.grammar.Production(nltk.grammar.Nonterminal(rule.lhs()), table[(table.n,j)].symbols + [None])
		    table[(j-1,j)].add_entry({rule.lhs(): (new_rule, ((table.n,j), None))})

		#Iterate over all non-(nonterminal -> terminal) cells of table
		#Move up rows
		for i in reversed(xrange(0, j)):
		    #Analyze all reachable cells

		    #Reset new symbols list and children dict
		    symbols = []
		    probs = []
		    children = []

		    #Iterate over all possible children
		    for k in xrange(i+1, j):
				#Analyze all cells to left in row
				try:
				    left_possibilities = table[i,k].symbols
				except KeyError:
				    continue
				for left in left_possibilities:
				    #Analyze all cells below in column
				    try:
						down_possibilities = table[k,j].symbols
				    except KeyError:
						continue
				    for down in down_possibilities:
						#Determine all possible nonterminals A in rules A -> B C where B is left and C is down
						rules = [x for x in grammar.productions() if x.rhs() == (left, down)]
						if rules != []:
						    for rule in rules:
								if rule.lhs() not in symbols:
								    if not (rule.lhs() == grammar.start() and (i,j) != (0,table.n)):
										symbols.append(rule.lhs())
										new_rule = nltk.grammar.Production(rule.lhs(), [left, down])
										children.append({rule.lhs(): (new_rule, ((i,k),(k,j)))})
								else:
								    new_rule = nltk.grammar.Production(rule.lhs(), [left, down])
								    children.append({rule.lhs(): (new_rule, ((i,k),(k,j)))})

		    #Add new entry to table
		    if symbols != []:
				table[(i,j)] = TableEntry()
				for child in children:
				    table[(i,j)].add_entry(child)
	    
    return table

def prob_cky_parse(grammar, tokens):
    #Initialize parse table
    table = ParseTable(tokens)

    #Move left to right across table
    for j in xrange(1, table.n + 1):
		#Add nonterminal symbols that correspond to terminal symbol
		rules = grammar.productions(rhs = tokens[j-1])
		table[(j-1,j)] = TableEntry()
		for rule in rules:
		    new_rule = nltk.grammar.Production(nltk.grammar.Nonterminal(rule.lhs()), table[(table.n,j)].symbols + [None])
		    table[(j-1,j)].add_entry({rule.lhs(): (new_rule, ((table.n,j), None))})
		    table[(j-1,j)].set_prob(rule.lhs(), rule.prob())

		#Iterate over all non-(nonterminal -> terminal) cells of table
		#Move up rows
		for i in reversed(xrange(0, j)):
		    #Analyze all reachable cells

		    #Reset new symbols list and children dict
		    symbols = []
		    probs = []
		    children = []

		    #Iterate over all possible children
		    for k in xrange(i+1, j):
				#Analyze all cells to left in row
				try:
				    left_possibilities = table[i,k].symbols
				except KeyError:
				    continue
				for left in left_possibilities:
				    #Analyze all cells below in column
				    try:
						down_possibilities = table[k,j].symbols
				    except KeyError:
						continue
				    for down in down_possibilities:
						#Determine all possible nonterminals A in rules A -> B C where B is left and C is down
						rules = [x for x in grammar.productions() if x.rhs() == (left, down)]
						if rules != []:
						    for rule in rules:
								new_prob = rule.prob() * table[(i,k)].prob(left) * table[(k,j)].prob(down)
								try:
								    new_rule = nltk.grammar.Production(rule.lhs(), [left, down])
								    table[(i,j)] = TableEntry()
								    table[(i,j)].add_entry({rule.lhs(): (new_rule, ((i,k),(k,j)), new_prob)})
								    table[(i,j)].set_prob(rule.lhs(), table[(i,j)].prob(rule.lhs()) + new_prob)
								except KeyError:
								    new_rule = nltk.grammar.Production(rule.lhs(), [left, down])
								    table[(i,j)] = TableEntry()
								    table[(i,j)].add_entry({rule.lhs(): (new_rule, ((i,k),(k,j)))})
								    table[(i,j)].set_prob(rule.lhs(), new_prob)
    return table
