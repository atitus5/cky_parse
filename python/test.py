import nltk
import cky
import sys

#Check if grammar argument is supplied
if len(sys.argv) < 2:
    raise Exception("Please specify a grammar to be used as the first argument.")
else:
    #Load context-free toy grammar (already in Chomsky Normal Form)
    grammar = nltk.data.load(sys.argv[1])

    #Receive sentence to parse
    #sentence = raw_input('Please enter a sentence:\n')

    #Use predefined sentence
    sentence = "the dog chased the cat on the dog" 

    #Split sentence into words
    tokens = sentence.split(" ")

    #Check if grammar can parse sentence
    grammar.check_coverage(tokens)

    #Check if grammar is PCFG, else run as CFG
    if isinstance(grammar, nltk.grammar.PCFG):
		parse_table = cky.prob_cky_parse(grammar, tokens)
    else:
		parse_table = cky.cky_parse(grammar, tokens)
    print parse_table
    print "Done!"
