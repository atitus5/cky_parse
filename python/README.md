CKY Parsing using Python
=======================

This version uses Python 2.7.6 and nltk 3.0.0.
Please note for future versions.

This directory contains several different files useful for conducting CKY
parsing in Python. Please note that it is recommended to use the Julia version
instead, as this has no support for parallelization or dense grammars.

Contents
--------

  * ` cky.py `         - Probabilistic CKY parsing and other NLP utilities 
  * ` test.py `        - Simple testing suite for CKY parsing
  * ` toy.cfg `        - Simple toy CFG in CNF
  * ` toy.pcfg `       - Simple toy PCFG in CNF
  * ` README.md `      - This file.

Usage
-----

To parse, run ` python test.py < grammar file desired > `

One can either wait for a user-input sentence or use a predefined sentence by setting the
` sentence ` variable, as demonstrated by default with a short sentence for the
` toy.cfg ` and ` toy.pcfg ` files.
